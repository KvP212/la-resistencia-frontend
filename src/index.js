import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import './static/fonts/doctor-glitch.ttf';


//Redux
import { Provider as ReduxProvider } from 'react-redux'
import generateStore from './redux/store'

let store = generateStore()

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#6fbf73',
      main: "#4caf50",
      dark: '#357a38',
      contrastText: '#fff'
    },
    secondary: {
      light: '#33eb91',
      main: "#00e676",
      dark: '#00a152',
      contrastText: '#fff'
    }
  },
  typography: {
    allVariants: {color: "#fff"}
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        body: {
          background: 'linear-gradient(90deg, #7e3a91 0.5%, #350044 99.5%)',
          color: '#ffffff'
        }
      }
    },
    MuiTab: {
      textColorPrimary: {
        color: "#ffffff"
      }
    },
    MuiDialogContentText: {
      root: {
        color: '#fff'
      }
    },
    MuiPaper: {
      root: {
        backgroundColor: '#350044',
      }
    },
    MuiDialogTitle: {
      root: {
        color: '#fff'
      }
    }
  }
})

ReactDOM.render(
  <ReduxProvider store={store}>
    
    <ThemeProvider theme={theme}>

      <CssBaseline />

      <Container maxWidth="lg">
          <App />
      </Container>

    </ThemeProvider>

  </ReduxProvider>,
  
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
