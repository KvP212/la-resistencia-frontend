import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import PeopleIcon from '@material-ui/icons/People';
import AssignmentIcon from '@material-ui/icons/Assignment';
import InfoIcon from '@material-ui/icons/Info';

import PropTypes from 'prop-types';
import bannerIMG from './static/img/BANNER.jpg';
import CardMedia from '@material-ui/core/CardMedia';

import HomePage from './components/HomePage'
import GroupPage from './components/GroupPage'
import { InfoPage } from './components/InfoPage';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    backgroundColor: '#350044'
  },
  bannerMedia: {
    height: 160
  },
})

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        children
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function App() {

  const classes = useStyles();
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="App">

      <CardMedia
        className={classes.bannerMedia}
        image={bannerIMG}
      />
    
      <header className="App-header">

        <Paper square className={classes.root}>
          <Tabs
            value={value}
            onChange={handleChange}
            variant="fullWidth"
            indicatorColor="primary"
            textColor="primary"
            aria-label="icon tabs example"
          >

            <Tab icon={<AssignmentIcon fontSize="large" />} aria-label="home" {...a11yProps(0)} />
            <Tab icon={<PeopleIcon fontSize="large" />} aria-label="groups" {...a11yProps(1)}/>
            <Tab icon={<InfoIcon />} aria-label="info" {...a11yProps(2)} />
            
          </Tabs>
        </Paper>
      
      </header>


      <div>
        <TabPanel value={value} index={0}>
          <HomePage/>
        </TabPanel>
        
        <TabPanel value={value} index={1}>
          <GroupPage />
        </TabPanel>
        
        <TabPanel value={value} index={2}>
          <InfoPage />
        </TabPanel>
      </div>
      
    </div>
  );
}

export default App;
