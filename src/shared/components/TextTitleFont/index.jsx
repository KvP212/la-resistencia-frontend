import React from 'react';
import { makeStyles, responsiveFontSizes, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

let theme = createMuiTheme()
theme = responsiveFontSizes(theme)

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    fontFamily: 'DoctorGlitch',
    letterSpacing: 3.5,
    wordSpacing: 5,
    padding: 8
  },
});

const TextTitleFont = (props) => {

  const { customText } = props;
  const classes = useStyles();

  return (

    <ThemeProvider theme={theme}>
      
      <Typography className={classes.root} variant="h2" >
        {customText}
      </Typography>

    </ThemeProvider>

  )
}

export default TextTitleFont;