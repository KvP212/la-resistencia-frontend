import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import CircularProgress from '@material-ui/core/CircularProgress';

//Store
import { useSelector, useDispatch } from 'react-redux'
import { storage, db } from '../../../firebase'


//CustomTextfield
const CssTextField = withStyles({
  root: {
    
    '& .MuiInputBase-input': {
      color: '#fff',
    },

    '& label': {
      color: 'gray',
    },

    '& label.Mui-focused': {
      color: 'primary',
    },

    '& .MuiInput-underline:after': {
      borderBottomColor: 'primary',
    },

    '& .MuiInput-underline:before': {
      borderBottomColor: 'gray',
    },
    
  }
})(TextField);

const useStyles = makeStyles((theme) => ({
  root: {
    margin: '15px 0px 0px 0px',
  },
  section2: {
    margin: '50px 0px 0px 0px'
  }, 
  input: {
    display: 'none',
  },
}));

//func Aux
const getGroupNum = teams => {

  //Asignar Grupo
  let numGroup = 0
  let count1 = 0
  let count2 = 0
  let count3 = 0
  let count4 = 0

  for (let team of teams) {

    switch (team.data.group) {
      case 1: 
        count1 = count1 + 1
        break;

      case 2:
        count2 = count2 + 1
        break;

      case 3:
        count3 = count3 + 1
        break;

      case 4:
        count4 = count4 + 1
        break;

      default:
        break;
    }
  }

  let asignado = true

  while (asignado) {
    
    switch (Math.floor(Math.random() * 4) + 1) {
      case 1:
        if (count1 < 16) {
          numGroup = 1
          asignado = false
        }
        break;

      case 2:
        if (count2 < 16) {
          numGroup = 2
          asignado = false
        }
        break;

      case 3:
        if (count3 < 16) {
          numGroup = 3
          asignado = false
        }
        break;

      case 4:
        if (count4 < 16) {
          numGroup = 4
          asignado = false
        }
        break;
      default:
        asignado = false
        break;
    }

  }
  
  return numGroup
}

export const FormDialogSus = () => {

  //Store
  const open = useSelector( state => state.formSus.open )
  const fetching = useSelector(state => state.teams.fetching)
  const teams = useSelector(state => state.teams.teams)
  const dispatch = useDispatch()

  const classes = useStyles()

  //LocalState
  const [team, setTeam] = useState('')
  const [errteam, setErrteam] = useState(false)

  const [teamTag, setTeamTag] = useState('')
  const [errTeamTag, setErrTeamTag] = useState(false)

  const [imgFile, setImgFile] = useState('')

  const [player1, setPlayer1] = useState('')
  const [errPlayer1, setErrPlayer1] = useState(false)

  const [player2, setPlayer2] = useState('')
  const [errPlayer2, setErrPlayer2] = useState(false)

  const [player3, setPlayer3] = useState('')
  const [errPlayer3, setErrPlayer3] = useState(false)

  const [player4, setPlayer4] = useState('')
  const [errPlayer4, setErrPlayer4] = useState(false)

  const [player5, setPlayer5] = useState('')
  const [errPlayer5, setErrPlayer5] = useState(false)

  const [player6, setPlayer6] = useState('')
  const [errPlayer6, setErrPlayer6] = useState(false)

  const maxAllowedSize = 4 * 1024 * 1024;
  
  const handleClose = () => {

    setTeam('')
    setTeamTag('')
    setImgFile('')
    setPlayer1('')
    setPlayer2('')
    setPlayer3('')
    setPlayer4('')
    setPlayer5('')
    setPlayer6('')

    dispatch({
      type: 'CLOSE'
    })
  };

  const handleSus = async () => {

    if (
      imgFile === '' ||
      imgFile === undefined ||
      imgFile.size >= maxAllowedSize ||
      team === '' ||
      teamTag === '' ||
      player1 === '' ||
      player2 === '' ||
      player3 === '' ||
      player4 === '' 
    ) {
      handleClose()
      return;
    }

    dispatch({
      type: 'GET_TEAMS'
    })

    const storageRef = storage.ref()
    let uploadTask = storageRef.child('images/' + imgFile.name).put(imgFile)

    uploadTask.on('state_changed',
      () => {},
      err => {
        //console.log(err.code)
        dispatch({ type: 'GET_TEAMS_ERROR' })
      },
      async () => {

        try {
          const downloadURL = await uploadTask.snapshot.ref.getDownloadURL()

          const groupNum = getGroupNum(teams)
          
          let saveTeam = {
            nameClan: team,
            tag: teamTag,
            img: downloadURL,
            group: groupNum,
            player1: player1,
            player2: player2,
            player3: player3,
            player4: player4,
            player5: player5,
            player6: player6
          }

          await db.add(saveTeam)
          //console.log('Document Written with ID: ', docRef.id)
          
          let newArray = []
          const querySnapshot = await db.get()

          querySnapshot.forEach(doc => {
            newArray.push({ id: doc.id, data: doc.data() })
          })

          dispatch({
            type: 'GET_TEAMS_SUCCESS',
            payload: newArray
          })
        
        } catch (error) {
          //console.log("Error adding document: ", error)
          dispatch({ type: 'GET_TEAMS_ERROR' })
        }

        handleClose()
      }
    )

  }

  const handleTeam = value => {
    
    if (team.length > 27) {
      setErrteam(true)
      setTeam('')
      return;
    }

    setErrteam(false)
    setTeam(value)

  }

  const handleTeamTag = value => {

    if (teamTag.length > 10) {
      setErrTeamTag(true)
      setTeamTag('')
      return;
    }

    setErrTeamTag(false)
    setTeamTag(value)
  }
  
  const handleImgFile = file => {
    setImgFile(file)
  }

  const handlePlayer1 = value => {

    if (player1.length > 27) {
      setErrPlayer1(true)
      setPlayer1('')
      return;
    }

    setErrPlayer1(false)
    setPlayer1(value)

  }

  const handlePlayer2 = value => {

    if (player2.length > 27) {
      setErrPlayer2(true)
      setPlayer2('')
      return;
    }

    setErrPlayer2(false)
    setPlayer2(value)

  }

  const handlePlayer3 = value => {

    if (player3.length > 27) {
      setErrPlayer3(true)
      setPlayer3('')
      return;
    }

    setErrPlayer3(false)
    setPlayer3(value)

  }

  const handlePlayer4 = value => {

    if (player4.length > 27) {
      setErrPlayer4(true)
      setPlayer4('')
      return;
    }

    setErrPlayer4(false)
    setPlayer4(value)

  }

  const handlePlayer5 = value => {

    if (player5.length > 27) {
      setErrPlayer5(true)
      setPlayer5('')
      return;
    }

    setErrPlayer5(false)
    setPlayer5(value)

  }

  const handlePlayer6 = value => {

    if (player6.length > 27) {
      setErrPlayer6(true)
      setPlayer6('')
      return;
    }

    setErrPlayer6(false)
    setPlayer6(value)

  }

  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">

        <DialogTitle id="form-dialog-title">Suscribete</DialogTitle>

        <DialogContent>
          
          <DialogContentText>
            Para participar en el nuevo torneo de La Resistencia completa el siguiente formulario.
          </DialogContentText>

          <CssTextField
            autoFocus
            margin="dense"
            id="team"
            label="Nombre del Clan (max. 27 caracteres)"
            type="text"
            fullWidth
            required={true}
            value={team}
            onChange={e => { handleTeam(e.target.value) }}
            error={errteam}
          />

          <CssTextField
            margin="dense"
            id="teamTag"
            label="Tag"
            type="text"
            fullWidth
            required={true}
            value={teamTag}
            onChange={e => { handleTeamTag(e.target.value) }}
            error={errTeamTag}
          />

          <div className={classes.root}>
            
            <input
              className={classes.input}
              accept="image/*"
              id="icon-button-file"
              type="file"
              onChange={e => { handleImgFile(e.target.files[0]) }}
            />
            
            <label htmlFor="icon-button-file">
              <IconButton color="primary" aria-label="upload picture" component="span">
                <PhotoCamera fontSize="large" />
              </IconButton>
            </label>
          </div>

          {
            imgFile === '' || imgFile === undefined ?
            (
              <DialogContentText>
                No has seleccionado ninguna imagen (Tamaño Max. 4MB)
              </DialogContentText>
            )
            :
            (
              <DialogContentText>
                {imgFile.name}
              </DialogContentText>
            )
          }

          <CssTextField
            margin="dense"
            id="teamPlayer1"
            label="Jugador"
            type="text"
            fullWidth
            required={true}
            value={player1}
            onChange={e => { handlePlayer1(e.target.value) }}
            error={errPlayer1}
          />

          <CssTextField
            margin="dense"
            id="teamPlayer2"
            label="Jugador"
            type="text"
            fullWidth
            required={true}
            value={player2}
            onChange={e => { handlePlayer2(e.target.value) }}
            error={errPlayer2}
          />

          <CssTextField
            margin="dense"
            id="teamPlayer3"
            label="Jugador"
            type="text"
            fullWidth
            required={true}
            value={player3}
            onChange={e => { handlePlayer3(e.target.value) }}
            error={errPlayer3}
          />

          <CssTextField
            margin="dense"
            id="teamPlayer4"
            label="Jugador"
            type="text"
            fullWidth
            required={true}
            value={player4}
            onChange={e => { handlePlayer4(e.target.value) }}
            error={errPlayer4}
          />


          <div className={classes.section2}>

            <DialogContentText>
              Reservas
            </DialogContentText>

            <CssTextField
              margin="dense"
              id="teamPlayer5"
              label="Jugador"
              type="text"
              fullWidth
              value={player5}
              onChange={e => { handlePlayer5(e.target.value) }}
              error={errPlayer5}
            />

            <CssTextField
              margin="dense"
              id="teamPlayer6"
              label="Jugador"
              type="text"
              fullWidth
              value={player6}
              onChange={e => { handlePlayer6(e.target.value) }}
              error={errPlayer6}
            />
            
          </div>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleSus} color="primary">
            Suscribete
          </Button>

          { fetching && <CircularProgress /> }
        </DialogActions>
      </Dialog>
    </div>
  );
}