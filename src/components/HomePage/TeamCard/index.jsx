import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

//DialogPlayers
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';

import PersonIcon from '@material-ui/icons/Person';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { green } from '@material-ui/core/colors';

import Divider from '@material-ui/core/Divider';


const useStyles = makeStyles({
  root: {
    width: 140,
    margin: 8,
    backgroundColor: '#121212',
    color: '#ffffff'
  },

  media: {
    height: 100,
    backgroundSize: 'contain'
  },

  avatar: {
    backgroundColor: green[100],
    color: green[600],
  },

  dividerLine: {
    backgroundColor: green[600]
  }
});


const TeamCard = (props) => {
  const classes = useStyles();
  
  const { id, tag, nameClan, img, players } = props
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen( false )
  }

  const handleOpenPlayers = () => {
    setOpen( true )
  }

  return (
    <>
    <Card className={classes.root}>
      <CardActionArea onClick={handleOpenPlayers}>
        
        <CardMedia
          className={classes.media}
          image={img || 'https://upload.wikimedia.org/wikipedia/commons/5/53/Escudo_vac%C3%ADo.png'}
          title={nameClan}
          key={id}
        />

        <CardContent>
          <Typography gutterBottom variant="caption" component="h2">
            { tag + ' ' + nameClan }
          </Typography>
          
        </CardContent>

      </CardActionArea>
      
      </Card>

      <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
        
        <DialogTitle id="simple-dialog-title">Jugadores</DialogTitle>
        
        <List>
          {
            players.map((player, index) => {

              return <div key={index}>
                {
                  player !== '' ?
                    (
                      <>
                        <Divider className={classes.dividerLine} />
                        <ListItem button >
                          <ListItemAvatar>
                            <Avatar size='sm' className={classes.avatar}>
                              <PersonIcon />
                            </Avatar>
                          </ListItemAvatar>
                          <ListItemText primary={player} />
                        </ListItem>
                        
                        
                      </>
                    )
                    :
                    (
                      <></>
                    )
                }
                
              </div>
            })
          }
          
        </List>

      </Dialog>
    </>
  )
}

export default TeamCard;