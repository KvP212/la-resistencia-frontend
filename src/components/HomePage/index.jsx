import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
import Button from '@material-ui/core/Button';

import TeamCard from "./TeamCard";
import TextTitleFont from '../../shared/components/TextTitleFont';


//Store
import { useDispatch, useSelector } from 'react-redux'


//Form
import { FormDialogSus } from './FormDialogSus';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  susButton: {
    margin: '15px 0px',
    display: 'flex',
    justifyContent: 'center'
  }
});

const HomePage = () => {
  
  const classes = useStyles();

  //Store
  const teams = useSelector(state => state.teams.teams)
  const dispatch = useDispatch()

  return (

    <>
      
      <TextTitleFont customText={"Torneo La Resistencia"} />
      <TextTitleFont customText={"14-08-2020"} />

      <div className={classes.susButton}>
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={() => dispatch({ type: 'OPEN' })}
          disabled={teams.length >= 64 ? true : false}
        >
          Suscribete [ Slots: {64 - teams.length } ]
        </Button>
      </div>

      {/**Dialog Modal*/}
      <FormDialogSus />

      <div className={classes.root}>
        {
          teams.length !== 0 ?
            
            (
              teams.map((currentValue) => {

                let players = [
                  currentValue.data.player1,
                  currentValue.data.player2,
                  currentValue.data.player3,
                  currentValue.data.player4,
                  currentValue.data.player5,
                  currentValue.data.player6,
                ]

                return <TeamCard
                  key={currentValue.id}
                  id={currentValue.id}
                  tag={currentValue.data.tag}
                  nameClan={currentValue.data.nameClan}
                  img={currentValue.data.img}
                  players={players}
                />
              })
            )
            :
            (
              <div>

                <Skeleton
                  variant="rect"
                  width={180}
                  height={180}

                  animation="wave"
                />
                <Skeleton variant="text" animation="wave" />
                <Skeleton variant="text" animation="wave" />

              </div>
            )
        }
      </div>  

    </>
    
  )
}

export default HomePage;