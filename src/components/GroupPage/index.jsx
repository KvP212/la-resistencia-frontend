import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextTitleFont from '../../shared/components/TextTitleFont'

import TeamCard from '../HomePage/TeamCard'

//Store
import { useSelector } from 'react-redux'

const useStyles = makeStyles({
  root: {
    marginBottom: 50,
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  }
  
});


const GroupPage = () => {
  const classes = useStyles();

  const handleGroup = (num) => {
    const groupFilter = teams.filter(team => team.data.group === num)

    return groupFilter
  }

  //Store
  const teams = useSelector(state => state.teams.teams)

  return (
    <>
      <TextTitleFont customText={"Grupo A"} />
      <div className={classes.root}>
        {
          handleGroup(1).map(currentValue => {

            let players = [
              currentValue.data.player1,
              currentValue.data.player2,
              currentValue.data.player3,
              currentValue.data.player4,
              currentValue.data.player5,
              currentValue.data.player6,
            ]

            return <TeamCard
              key={currentValue.id}
              id={currentValue.id}
              tag={currentValue.data.tag}
              nameClan={currentValue.data.nameClan}
              img={currentValue.data.img}
              players={players}
            />
          })
        }
      </div>


      <TextTitleFont customText={"Grupo B"} />
      <div className={classes.root}>
        {
          handleGroup(2).map(currentValue => {

            let players = [
              currentValue.data.player1,
              currentValue.data.player2,
              currentValue.data.player3,
              currentValue.data.player4,
              currentValue.data.player5,
              currentValue.data.player6,
            ]

            return <TeamCard
              key={currentValue.id}
              id={currentValue.id}
              tag={currentValue.data.tag}
              nameClan={currentValue.data.nameClan}
              img={currentValue.data.img}
              players={players}
            />
          })
        }
      </div>

      
      <TextTitleFont customText={"Grupo C"} />
      <div className={classes.root}>
        {
          handleGroup(3).map(currentValue => {

            let players = [
              currentValue.data.player1,
              currentValue.data.player2,
              currentValue.data.player3,
              currentValue.data.player4,
              currentValue.data.player5,
              currentValue.data.player6,
            ]

            return <TeamCard
              key={currentValue.id}
              id={currentValue.id}
              tag={currentValue.data.tag}
              nameClan={currentValue.data.nameClan}
              img={currentValue.data.img}
              players={players}
            />
          })
        }
      </div>

      
      <TextTitleFont customText={"Grupo D"} />
      <div className={classes.root}>
        {
          handleGroup(4).map(currentValue => {

            let players = [
              currentValue.data.player1,
              currentValue.data.player2,
              currentValue.data.player3,
              currentValue.data.player4,
              currentValue.data.player5,
              currentValue.data.player6,
            ]
            
            return <TeamCard
              key={currentValue.id}
              id={currentValue.id}
              tag={currentValue.data.tag}
              nameClan={currentValue.data.nameClan}
              img={currentValue.data.img}
              players={players}
            />
          })
        }
      </div>

    </>
  )
}

export default GroupPage;