import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import Fab from '@material-ui/core/Fab';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import SubscriptionsIcon from '@material-ui/icons/Subscriptions';

//Custom JSX Elements
import TextTitleFont from '../../shared/components/TextTitleFont'


const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  customFab: {
    margin: 30,
    width: 100,
    height: 100
    
  },
  media: {
    margin: 20
  },
});


export const InfoPage = () => {
  const classes = useStyles();

  return (
    <>
      <TextTitleFont customText={"La Resistencia"} />

      <div className={classes.root}>

        <Fab className={classes.customFab} color="primary" aria-label="fb" onClick={() => { window.location.href = 'https://www.facebook.com/La-Resistencia-103154501437293/'} } >
          <FacebookIcon style={ {fontSize: 60} } />
        </Fab>

        <Fab className={classes.customFab} color="primary" aria-label="Instagram" onClick={() => { window.location.href = 'https://instagram.com/lresistenciaesports?igshid=kzk4g47jfakb'} } >
          <InstagramIcon style={{ fontSize: 60 }} />
        </Fab>

        <Fab className={classes.customFab} color="primary" aria-label="youtube" onClick={() => { window.location.href = 'https://www.youtube.com/channel/UCKFty8OE4ye71hhCzMge9rw/videos'} }>
          <SubscriptionsIcon style={{ fontSize: 60 }} />
        </Fab>

      </div>

      <div className={classes.media}>
        <iframe
          title='UltimaScrim'
          width="100%"
          height="315"
          src="https://www.youtube.com/embed/cEoiUvwQKbE"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen>
        </iframe>
      </div>
      

    </>
  )
}