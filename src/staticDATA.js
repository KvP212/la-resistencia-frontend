import logoLR from './static/img/LogoLR.png';

export const data = [
  {
    id: '1',
    nameClan: 'Resistencia',
    tag: '『LR』',
    img: logoLR,
    group: null,
    memberName1: 'Kev Pineda',
    memberName2: 'Fer Pineda',
    memberName3: 'Kev Pineda',
    memberName4: 'Kev Pineda',
    memberName5: 'Kev Pineda',
    memberName6: 'Kev Pineda'
  },

  {
    id: '2',
    nameClan: 'La Resistencia',
    tag: '『LR』',
    img: 'https://w7.pngwing.com/pngs/724/759/png-transparent-apple-logo-apple-computer-icons-apple-logo-heart-computer-logo.png',
    group: null,
    memberName1: 'Kev Pineda',
    memberName2: 'Fer Pineda',
    memberName3: 'Kev Pineda',
    memberName4: 'Kev Pineda',
    memberName5: 'Kev Pineda',
    memberName6: 'Kev Pineda'
  },

  {
    id: '3',
    nameClan: 'La Resistencia Resistencia',
    tag: '『LR』',
    img: 'https://w7.pngwing.com/pngs/724/759/png-transparent-apple-logo-apple-computer-icons-apple-logo-heart-computer-logo.png',
    group: null,
    memberName1: 'Kev Pineda',
    memberName2: 'Fer Pineda',
    memberName3: 'Kev Pineda',
    memberName4: 'Kev Pineda',
    memberName5: 'Kev Pineda',
    memberName6: 'Kev Pineda'
  },

  {
    id: '4',
    nameClan: 'La Resistencia',
    tag: '『LR』',
    img: 'https://w7.pngwing.com/pngs/724/759/png-transparent-apple-logo-apple-computer-icons-apple-logo-heart-computer-logo.png',
    group: null,
    memberName1: 'Kev Pineda',
    memberName2: 'Fer Pineda',
    memberName3: 'Kev Pineda',
    memberName4: 'Kev Pineda',
    memberName5: 'Kev Pineda',
    memberName6: 'Kev Pineda'
  },

  {
    id: '5',
    nameClan: 'La Resistencia',
    tag: '『LR』',
    img: 'https://w7.pngwing.com/pngs/724/759/png-transparent-apple-logo-apple-computer-icons-apple-logo-heart-computer-logo.png',
    group: null,
    memberName1: 'Kev Pineda',
    memberName2: 'Fer Pineda',
    memberName3: 'Kev Pineda',
    memberName4: 'Kev Pineda',
    memberName5: 'Kev Pineda',
    memberName6: 'Kev Pineda'
  },

  {
    id: '6',
    nameClan: 'La Resistencia',
    tag: '『LR』',
    img: 'https://w7.pngwing.com/pngs/724/759/png-transparent-apple-logo-apple-computer-icons-apple-logo-heart-computer-logo.png',
    group: null,
    memberName1: 'Kev Pineda',
    memberName2: 'Fer Pineda',
    memberName3: 'Kev Pineda',
    memberName4: 'Kev Pineda',
    memberName5: 'Kev Pineda',
    memberName6: 'Kev Pineda'
  }



]