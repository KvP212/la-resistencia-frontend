import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

let firebaseConfig = {
  apiKey: "AIzaSyCPrDUyXYcpTrvxy6GTl6LnMDOFFVHMcm4",
  authDomain: "la-resistencia-e3dfa.firebaseapp.com",
  databaseURL: "https://la-resistencia-e3dfa.firebaseio.com",
  projectId: "la-resistencia-e3dfa",
  storageBucket: "la-resistencia-e3dfa.appspot.com",
  messagingSenderId: "795221236675",
  appId: "1:795221236675:web:1da11acf68bc54a1117b76",
  measurementId: "G-TM0Z05QCG7"
};
// Initialize Firebase
const fireApp = firebase.initializeApp(firebaseConfig);

//firestore
export const db = fireApp.firestore().collection('teams');

//firestorage
export const storage = fireApp.storage();