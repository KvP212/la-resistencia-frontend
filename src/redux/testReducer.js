// constants
let initialData = {
  textoPrueba: "false",
  fetching: false
}


let GET_CHARACTERS = 'GET_CHARACTERS'
let GET_CHARACTERS_SUCCESS = 'GET_CHARACTERS_SUCCESS'


//reducer
export default function reducer(state = initialData, action) {
  switch (action.type) {
    case GET_CHARACTERS: 
      return { ...state, fetching: true }
    
    case GET_CHARACTERS_SUCCESS:
      return { ...state, fetching: false }
    
    default:
      return state
  }
}

export let retreiveFavs = () => (dispatch, getState) => {

  dispatch({
    type: GET_CHARACTERS
  })

  return dispatch({
    type: GET_CHARACTERS_SUCCESS,
  })
}