import { db } from '../firebase'

// constants
let initialData = {
  teams: [],
  fetching: false
}


let GET_TEAMS = 'GET_TEAMS'
let GET_TEAMS_SUCCESS = 'GET_TEAMS_SUCCESS'
let GET_TEAMS_ERROR = 'GET_TEAMS_ERROR'


//reducer
export default function reducer(state = initialData, action) {
  switch (action.type) {
    case GET_TEAMS:
      return { ...state, fetching: true }

    case GET_TEAMS_SUCCESS:
      return { ...state, fetching: false, teams: action.payload }

    case GET_TEAMS_ERROR:
      return { ...state, fetching: false }
    
    default:
      return state
  }
}

export const getInitialTeams = () => async(dispatch, getState) => {
  try {

    let newArray = []
    const querySnapshot = await db.get();

    querySnapshot.forEach( doc => {
      newArray.push({ id: doc.id, data: doc.data() })
    })

    dispatch({
      type: GET_TEAMS_SUCCESS,
      payload: newArray
    })

  } catch (error) {
    //console.error("Error GET: ", error);
    dispatch({
      type: GET_TEAMS_ERROR
    })
  }
}