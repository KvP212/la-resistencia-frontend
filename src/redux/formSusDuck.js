//import 'firebase/firestore'
//let db = firebase.firestore().collection('teams')

// constants
let initialData = {
  open: false
}

let OPEN = 'OPEN'
let CLOSE = 'CLOSE'


//reducer
export default function reducer(state = initialData, action) {
  switch (action.type) {
    case OPEN:
      return { ...state, open: true }

    case CLOSE:
      return { ...state, open: false }

    default:
      return state
  }
}