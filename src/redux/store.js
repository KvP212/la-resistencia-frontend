import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

//Ducks
import formSusReducer from './formSusDuck'
import teamsReducer, { getInitialTeams } from './teamsDuck'

let rootReducer = combineReducers({
  teams: teamsReducer,
  formSus: formSusReducer
})

export default function generateStore() {
  
  let store = createStore(
    rootReducer,
    compose(applyMiddleware(thunk))
  )

  getInitialTeams()(store.dispatch, store.getState)

  return store
}